package com.esightcorp.hmd.lib.custom_dialog_lib;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.esightcorp.hmd.comp.custom_dialog_lib.R;
import com.esightcorp.hmd.comp.custom_dialog_lib.databinding.CustomDialogBinding;


public class CustomDialogView extends Dialog {

    private VelocityTracker velocityTracker = null;
    private static float init_x = 0;
    private static float init_y = 0;

    public Button pos, neg;
    private int textSizeIndex;
    private TextView title;
    private TextView desc;

    private Float[] titleSizes = {37.3f , 50.7f, 64.1f};
    private Float[] descSizes = {23f, 37.3f, 50.7f};
    private Float[] btnSizes = {23f, 37.3f, 45.7f};


    public CustomDialogView(@NonNull Context context, @NonNull CustomDialog vm, final ButtonClickInterface bci, String textSize) {
        super(context);

        setTextSize(textSize);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        CustomDialogBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.custom_dialog, null, false );
        binding.setDialogVM(vm);
        View root = binding.getRoot();
        binding.executePendingBindings();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setContentView(root);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.getId()== R.id.btnPos) {
                    bci.positiveButtonClicked();
                    CustomDialogView.this.dismiss();
                }

                if (v.getId() == R.id.btnNeg) {
                    bci.negativeButtonClicked();
                    CustomDialogView.this.dismiss();
                }
            }
        };

        pos = findViewById(R.id.btnPos);
        pos.setOnClickListener(listener);
        pos.setTextSize(descSizes[textSizeIndex]);

        neg = findViewById(R.id.btnNeg);
        neg.setOnClickListener(listener);
        neg.setTextSize(descSizes[textSizeIndex]);

        title = findViewById(R.id.title);
        title.setTextSize(titleSizes[textSizeIndex]);

        desc = findViewById(R.id.description);
        desc.setTextSize(descSizes[textSizeIndex]);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if(velocityTracker == null) {
                    velocityTracker = VelocityTracker.obtain();
                } else {
                    velocityTracker.clear();
                }
                velocityTracker.addMovement(event);
                init_x = event.getX();
                init_y = event.getY();
                break;

            case MotionEvent.ACTION_MOVE:
                velocityTracker.addMovement(event);
                float delta_x = event.getX() - init_x;
                float delta_y = event.getY() - init_y;

                if (Math.abs(delta_y) > (4 * Math.abs(delta_x))) {
                    velocityTracker.computeCurrentVelocity(1);

                    if (delta_y > 0){
                        decreaseTextSize();
                    }
                    else if (delta_y < 0) {
                        increaseTextSize();
                    }

                    init_x = event.getX();
                    init_y = event.getY();
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                velocityTracker.recycle();
                velocityTracker = null;
                break;
        }
        return true;
    }

    public void decreaseTextSize() {

        if (textSizeIndex != 0){
            textSizeIndex = textSizeIndex -1;
        }
        title.setTextSize(titleSizes[textSizeIndex]);
        desc.setTextSize(descSizes[textSizeIndex]);
        neg.setTextSize(btnSizes[textSizeIndex]);
        pos.setTextSize(btnSizes[textSizeIndex]);

    }

    public void increaseTextSize() {

        if (textSizeIndex != 2){
            textSizeIndex = textSizeIndex +1;
        }
        title.setTextSize(titleSizes[textSizeIndex]);
        desc.setTextSize(descSizes[textSizeIndex]);
        neg.setTextSize(btnSizes[textSizeIndex]);
        pos.setTextSize(btnSizes[textSizeIndex]);

    }

    public void setTextSize(String s){
        switch(s) {
            case "S":
                textSizeIndex = 0;
                break;
            case "M":
                textSizeIndex = 1;
                break;
            case "L":
                textSizeIndex = 2;
                break;
            default:
                break;
        }
    }


    public interface ButtonClickInterface{
        void positiveButtonClicked();
        void negativeButtonClicked();
    }
}
