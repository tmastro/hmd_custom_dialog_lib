package com.esightcorp.hmd.lib.custom_dialog_lib;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import com.esightcorp.hmd.comp.custom_dialog_lib.R;




public class CustomDialog extends BaseObservable {
    private String TAG = "TOM";
    private Context mContext;
    private String dialogButtonPos;
    private String dialogButtonNeg;
    private String description;
    private String dialogTitle;
    private int cancellable ;

    public CustomDialog(Context context){
        mContext = context;
    }

    // Display Dialog No Cancel Button
    public void displayDialog(String title, String description, String positiveText ,
                              String color, String textSize,
                              CustomDialogView.ButtonClickInterface buttonInterface) {
        setTheme(color);
        setCancellable(View.GONE);
        setDialogButtonPos(positiveText);
        setDescription(description);
        setDialogTitle(title);
        
        CustomDialogView dialog = new CustomDialogView(mContext, this, buttonInterface, textSize);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_DIALOG);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        dialog.show();

    }

    // Display Dialog Cancel Button
    public void displayDialog(String title, String description, String positiveText, String negativeText,
                              String color, String textSize,
                              CustomDialogView.ButtonClickInterface buttonInterface) {
        setTheme(color);
        setCancellable(View.VISIBLE);
        setDialogButtonPos(positiveText);
        setDialogButtonNeg(negativeText);
        setDescription(description);
        setDialogTitle(title);

        CustomDialogView dialog = new CustomDialogView(mContext, this, buttonInterface, textSize);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_DIALOG);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        dialog.show();
    }


    @Bindable
    public String getDialogTitle(){
        return dialogTitle;
    }

    public void setDialogTitle(String s) {
        dialogTitle = s;
        notifyPropertyChanged(BR.dialogTitle);
    }

    @Bindable
    public Integer getCancellable() {return cancellable;}

    public void setCancellable(Integer i) {
        cancellable = i;
        notifyPropertyChanged(BR.cancellable);
    }

    @Bindable
    public String getDialogButtonPos() {return dialogButtonPos;}

    public void setDialogButtonPos(String s) {
        dialogButtonPos = s;
        notifyPropertyChanged(BR.dialogButtonPos);
    }

    @Bindable
    public String getDialogButtonNeg() {return dialogButtonNeg;}

    public void setDialogButtonNeg(String s) {
        dialogButtonNeg = s;
        notifyPropertyChanged(BR.dialogButtonNeg);
    }

    @Bindable
    public String getDescription() {return description;}

    public void setDescription(String s) {
        description = s;
        notifyPropertyChanged(BR.description);
    }

    public void setTheme(String s){
        switch(s) {
            case "0":
                mContext.setTheme(R.style.ThemeBlackWhite);

                break;
            case "1":
                mContext.setTheme(R.style.ThemeWhiteBlack);
                break;
            case "2":
                mContext.setTheme(R.style.ThemeBlueYellow);
                break;
            case "3":
                mContext.setTheme(R.style.ThemeYellowBlue);
                break;
            default:
                break;
        }
    }



}
